import Choice from '../../src/models/Choice';

import chai, { should, expect } from 'chai';
import { describe, it } from 'mocha';
should();

describe('Choice', () => {
  describe('numberOfResponse', () => {
    it('should return length equal 1', () => {
      const dummyChoice = new Choice({
        "desc": "idk",
        "userIds": [
          "5988fc278c1c6d3330a4475b",
        ]
      });
      expect(dummyChoice.numberOfResponse).to.equal(1);
    });

    it('should return length equal 3', () => {
      const dummyChoice = new Choice({
        "desc": "idk",
        "userIds": [
          "5988fc278c1c6d3330a4475b",
          "5988fc278c1c6d3330a4475c",
          "5988fc278c1c6d3330a4475d",
        ]
      });
      expect(dummyChoice.numberOfResponse).to.equal(3);
    });
  });
});