import Poll from '../../src/models/Poll';
import Choice from '../../src/models/Choice';

import chai, { should, expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
should();

describe('Poll', () => {
  let poll;
  let choices;
  beforeEach(() => {
    const dummyChoices = [{
      "desc": "Yes",
      "userIds": [
        "5988fc278c1c6d3330a4475b",
        "12cfv4vt4tv555ttt444t5vub",
        "5988fc278c1c6d3330a4475b",
        "12cfv4vt4tv555ttt444t5vub",
        "5988fc278c1c6d3330a4475b",
      ]
    }, {
      "desc": "No",
      "userIds": []
    }, {
      "desc": "Maybe",
      "userIds": [
        "5988fc278c1c6d3330a4475b",
      ]
    }, {
      "desc": "idk",
      "userIds": [
        "5988fc278c1c6d3330a4475b",
        "12cfv4vt4tv555ttt444t5vub",
        "5988fc278c1c6d3330a4475b",
        "12cfv4vt4tv555ttt444t5vub",
      ]
    }];
    choices = dummyChoices.map(dummy => new Choice(dummy));
    const dummyPoll = {
      question: 'Is this a question?',
      choices,
    };
    poll = new Poll(dummyPoll);
  });

  describe('computeResponsePercentage', () => {
    beforeEach(() => {
      poll.computeResponsePercentage();
    });

    it('computes first choice response percentage', () => {
      expect(choices[0].responsePercentage).to.be.closeTo(50, 0.01);
    });

    it('computes second choice response percentage', () => {
      expect(choices[1].responsePercentage).to.be.closeTo(0, 0.01);
    });

    it('computes third choice response percentage', () => {
      expect(choices[2].responsePercentage).to.be.closeTo(10, 0.01);
    });

    it('computes fourth choice response percentage', () => {
      expect(choices[3].responsePercentage).to.be.closeTo(40, 0.01);
    });
  });

  describe('totalResponses', () => {
    it('returns total responses', () => {
      poll.computeResponsePercentage();
      expect(poll.totalResponses).to.equal(10);
    });
  });
});