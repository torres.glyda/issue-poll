import censor from '../../src/hooks/censor';

import chai, { should, expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';

chai.should();

describe('censor hook', () => {
  let hook;
  beforeEach(() => {
    hook = {
      result: [
        { user: { username: 'myname' } },
      ],
    }
  });

  it('changes censors string', () => {
    const censorHook = () => censor('username', 'user')(hook);
    censorHook()
      .then((hook) => {
        return hook.result.map(res => res.user.username)
      }).then(username => username[0].should.equal('my**me'))
  });
});