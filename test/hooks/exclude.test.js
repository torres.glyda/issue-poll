import exclude from '../../src/hooks/exclude';

import chai, { should, expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import chaiAsPromised from 'chai-as-promised';

chai.should();
chai.use(chaiAsPromised);

describe('exclude hook', () => {
  let hook;
  beforeEach(() => {
    hook = {
      result: [{
        username: 'myname',
        password: 'passwerd',
      }],
    }
  });

  it('excludes property', () => {
    const excludeHook = () => exclude('password')(hook);
    const users = excludeHook()
      .then((hook) => {
        return hook.result;
      }).then(users => users[0])
    users.should.eventually.not.have.property('password');
  });
});