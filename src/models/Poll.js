import Model from '../models/Model';

class Poll extends Model {
  get totalResponses() {
    return this.choices.reduce((sum, choice) => sum + choice.numberOfResponse, 0);
  }

  computeResponsePercentage() {
    this.choices.forEach(choice => {
      choice.responsePercentage = this.totalResponses === 0 ? 0 : parseFloat(((choice.numberOfResponse / this.totalResponses) * 100).toFixed(2));
    });
  }
}

export default Poll;