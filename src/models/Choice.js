import Model from '../models/Model';

class Choice extends Model {
  get numberOfResponse() {
    return this.userIds.length;
  }

  set responsePercentage(responsePercentage) {
    this._responsePercentage = responsePercentage;
  }

  get responsePercentage() {
    return this._responsePercentage;
  }
}

export default Choice;