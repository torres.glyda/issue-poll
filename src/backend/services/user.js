import feathersMongo from 'feathers-mongodb';
import { hooks } from 'feathers-authentication-local';

const usersService = (db) => {
  return function() {
    const app = this;
    app.use('users', feathersMongo({
        Model: db.collection('users')
      }))
      .service('users')
      .hooks({
        before: {
          create: [
            hooks.hashPassword({ passwordField: 'password' }),
          ],
          update: [
            hooks.hashPassword({ passwordField: 'password' }),
          ],
          patch: [
            hooks.hashPassword({ passwordField: 'password' }),
          ],
        },
      })
  }
}

export default usersService;