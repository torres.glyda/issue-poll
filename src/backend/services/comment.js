import feathersMongo from 'feathers-mongodb';

import exclude from '../../hooks/exclude';
import populate from '../../hooks/populate';
import censor from '../../hooks/censor';

import mongodb from 'mongodb';
const ObjectId = mongodb.ObjectId;

const changeToObjectId = (prop) => (hook) => {
  const details = hook.type === 'after' ? 'result' : 'data';
  hook[details][prop] = ObjectId(hook[details][prop]);
  return Promise.resolve(hook);
}

const choicesService = (db) => {
  return function() {
    const app = this;
    app.use('/api/comments', feathersMongo({
        Model: db.collection('comments')
      }))
      .service('/api/comments')
      .hooks({
        before: {
          create: [
            changeToObjectId('pollId'),
            changeToObjectId('userId'),
            changeToObjectId('userChoiceId'),
          ]
        },
        after: {
          find: [
            populate('users', 'userId', 'user'),
            populate('api/choices', 'userChoiceId', 'userChoice'),
            exclude('userId'),
            exclude('userChoiceId'),
            censor('username', 'user'),
          ],
          create: [
            populate('users', 'userId', 'user'),
            populate('api/choices', 'userChoiceId', 'userChoice'),
            exclude('userId'),
            exclude('userChoiceId'),
            censor('username', 'user'),
          ],
        },
      });
  }
}

export default choicesService;