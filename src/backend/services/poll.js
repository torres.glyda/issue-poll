import feathersMongo from 'feathers-mongodb';

import exclude from '../../hooks/exclude';
import populate from '../../hooks/populate';

const pollsService = (db) => {
  return function() {
    const app = this;
    app.use('/api/polls', feathersMongo({
        Model: db.collection('polls')
      }))
      .service('/api/polls')
      .after({
        find: [
          populate('/api/choices', 'choicesIds', 'choices'),
          exclude('choicesIds'),
        ],
      });
  }
}

export default pollsService;