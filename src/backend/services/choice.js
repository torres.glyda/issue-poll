import feathersMongo from 'feathers-mongodb';
import Choice from '../../models/Choice';

import transform from '../../hooks/transform';

const choicesService = (db) => {
  return function() {
    const app = this;
    app.use('/api/choices', feathersMongo({
        Model: db.collection('choices')
      }))
      .service('/api/choices')
      .hooks({
        after: {
          find: [
            transform(Choice),
          ],
          patch: [
            transform(Choice),
          ]
        },
      });
  }
}

export default choicesService;