import user from './user';
import poll from './poll';
import choice from './choice';
import comment from './comment';
import authentication from './authentication';

const services = (db) => {
  return function() {
    const app = this;
    app.configure(authentication())
      .configure(user(db))
      .configure(poll(db))
      .configure(choice(db))
      .configure(comment(db));
  }
}

export default services;