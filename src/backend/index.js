import setupServer from './setupServer';

const startServer = async() => {
  const app = await setupServer();
  app.listen(app.get('port'), () => {
    console.log('App is running...');
  });
}

startServer();