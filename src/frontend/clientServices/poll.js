import Poll from '../../models/Poll';
import Choice from '../../models/Choice';

import transform from '../../hooks/transform';

const transFormChoices = () => (hook) => {
  const polls = hook.result;
  hook.result.forEach(poll => poll.choices = poll.choices.map(choice => new Choice(choice)));
}

const pollService = () => {
  return function() {
    const client = this;
    client.service('/api/polls')
      .hooks({
        after: {
          find: [
            transFormChoices(),
            transform(Poll),
          ],
        }
      });
  }
}

export default pollService;