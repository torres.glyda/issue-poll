import poll from './poll';

const clientServices = () => {
  return function() {
    const client = this;
    client.configure(poll());
  }
}

export default clientServices;