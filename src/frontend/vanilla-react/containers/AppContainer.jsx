import React, { Component } from 'react';
import App from '../components/App';

import client from '../../client';
const pollsService = client.service('/api/polls');

class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      polls: [],
      username: '',
      password: '',
    };
  }

  async getPolls() {
    try {
      const polls = await pollsService.find();
      this.setState({ polls });
    }
    catch (err) {
      console.log(err);
    }
  }

  async handleLogin() {
    const username = this.state.username;
    const password = this.state.password;
    try {
      const result = await client.authenticate({
        strategy: 'local',
        username,
        password,
      });
      const payload = await client.passport.verifyJWT(result.accessToken);
      const user = await client.service('users').get(payload.userId);
      this.setState({ user })
      client.set('user', user);
    }
    catch (error) {
      console.log(error);
    }
  }

  async handleLogout() {
    try {
      await client.logout();
      client.set('user', undefined);
      this.setState({ user: undefined })
    }
    catch (err) {
      console.log('LOGOUT_FAILED');
      console.log(err);
    }
  }

  updateInputValue(input) {
    return (evt) => {
      this.setState({
        [input]: evt.target.value
      });
    }
  }

  componentWillMount() {
    this.getPolls();
    this.setState({ user: client.get('user') })
  }

  render() {
    return (
      <App
        user={this.state.user}
        updateInputValue={(evt) => this.updateInputValue(evt)}
        handleLogin={() => this.handleLogin()}
        handleLogout={() => this.handleLogout()}
        polls={this.state.polls}
        classes={this.props.classes}
      />
    );
  }
}

export default AppContainer;