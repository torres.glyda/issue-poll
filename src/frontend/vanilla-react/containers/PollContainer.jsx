import React, { Component } from 'react';

import client from '../../client';
const pollsService = client.service('/api/polls');
const choicesService = client.service('/api/choices');

import Poll from '../components/Poll';

export default class PollContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			poll: {},
			user: {},
		}
	}

	selectChoice(poll, selectedChoice) {
		return () => {
			poll.choices.forEach((choice) => {
				const userIdIndex = choice.userIds.findIndex(userId => userId === this.state.user._id)
				if (userIdIndex !== -1) {
					choice.userIds.splice(userIdIndex, 1);
					this.updateChoice(choice._id, choice.userIds);
				} else if (choice._id === selectedChoice._id) {
					choice.userIds.push(this.state.user._id);
					this.updateChoice(choice._id, choice.userIds);
				}
			})
		}
	}

	async updateChoice(id, userIds) {
		try {
			await choicesService.patch(id, { userIds });
		} catch (error) {
			console.log(error)
		}
	}

	registerListeners() {
		choicesService.on('patched', updatedChoice => {
			const poll = this.state.poll;
			poll.choices.forEach(choice => {
				if (choice._id === updatedChoice._id) {
					Object.assign(choice, updatedChoice);
				}
				return choice;
			})
			poll.computeResponsePercentage();
			this.setState({ poll });
		})
	}

	removeListeners() {
		choicesService.off('patched');
	}

	componentWillMount() {
		this.registerListeners();
		this.props.poll.computeResponsePercentage();
		this.setState({ poll: this.props.poll });
		this.setState({ user: client.get('user') });
	}

	componentWillUnmount() {
		this.removeListeners();
	}

	render() {
		return (
			<Poll
				user={this.state.user}
				poll={this.state.poll}
				selectChoice={(poll, selectedChoice) => this.selectChoice(poll, selectedChoice)}
				classes={this.props.classes}
			/>
		);
	}
}