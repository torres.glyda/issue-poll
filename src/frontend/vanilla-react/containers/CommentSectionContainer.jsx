import React, { Component } from 'react';

import client from '../../client';
const commentsService = client.service('/api/comments');

import CommentSection from '../components/CommentSection';

class CommentSectionContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: [],
			comments: [],
		}
	}

	async getComments() {
		try {
			const comments = await commentsService.find();
			this.setState({ comments });
		}
		catch (err) {
			console.log(err);
		}
	}

	getUserChoice(poll) {
		const user = this.props.user;
		for (let i = 0; i < poll.choices.length; i++) {
			const choice = poll.choices[i];
			for (let j = 0; j < choice.userIds.length; j++) {
				const userId = choice.userIds[j];
				if (userId === user._id) {
					return choice;
					break;
				}
			}
		}
	}

	handleCommentSubmit(poll) {
		return (evt) => {
			const user = this.props.user;
			if (!!user) {
				const text = evt.target[0].value.trim();
				const userChoice = this.getUserChoice(poll);
				if (!text || !userChoice) {
					return;
				}
				this.addComment(poll._id, user._id, userChoice._id, text);
				evt.target[0].value = '';
			} else {
				alert('Login first!')
			}
		}
	}

	async addComment(pollId, userId, userChoiceId, text) {
		try {
			const choices = commentsService.create({ pollId, userId, userChoiceId, text });
		} catch (error) {
			console.log(error)
		}
	}

	registerListeners() {
		commentsService.on('created', comment => {
			const comments = this.state.comments.concat(comment);
			comments.push(comment);
			this.setState({ comments });
		})
	}

	removeListeners() {
		commentsService.off('created');
	}

	componentWillMount() {
		this.getComments();
		this.registerListeners();
	}

	componentWillUnmount() {
		this.removeListeners();
	}

	render() {
		return (
			<CommentSection
				classes={this.props.classes}
				poll={this.props.poll}
				handleCommentSubmit={(evt) => this.handleCommentSubmit(evt)}
				comments={this.state.comments}
			/>
		);
	}
}

export default CommentSectionContainer;