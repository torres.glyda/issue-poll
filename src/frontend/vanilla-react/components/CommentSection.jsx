import React from 'react';
import CommentForm from './CommentForm';
import CommentList from './CommentList';
import classNames from 'classnames';

const CommentSection = ({ poll, handleCommentSubmit, comments, classes }) => (
	<div className={classNames(classes.container, classes.commentSectionContainer)}>
		<CommentForm poll={poll} handleCommentSubmit={handleCommentSubmit} classes={classes} />
		{!!comments.length
			? <CommentList poll={poll} comments={comments} classes={classes} />
			: ''}
	</div>
);

export default CommentSection;