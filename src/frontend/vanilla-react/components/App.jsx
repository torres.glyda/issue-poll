import React from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';

import PollList from './PollList';
import PollContainer from '../containers/PollContainer';
import CommentSectionContainer from '../containers/CommentSectionContainer';

const App = ({ user, updateInputValue, handleLogin, handleLogout, polls, classes }) => (
	<Router >
		<div className={classes.center}>
			{!!user
				? <div>
					<div className={classes.textHeader}>Hi, {user.username}!</div>
					<button onClick={handleLogout}>LOGOUT</button >
				</div>
				: <div>
					<div className={classes.textHeader}>LOGIN</div>
					<span>username:</span><input type='text' onChange={updateInputValue('username')} />
					<span>password:</span><input type='password' onChange={updateInputValue('password')} />
					<button onClick={handleLogin}>LOGIN</button>
				</div>
			}
			<Route path='/' render={() => <PollList polls={polls} classes={classes} />} />
			{polls.map((poll, index) =>
				<Route key={poll._id} path={`/vanilla-poll-${index}`} render={() =>
					<div>
						<PollContainer poll={poll} classes={classes} className={classes.center} />
						<CommentSectionContainer user={user} poll={poll} classes={classes} />
					</div>
				} />
			)}
		</div>
	</Router>
);

export default App;