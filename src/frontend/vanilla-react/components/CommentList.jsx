import React from 'react';
import classNames from 'classnames';

const Comment = ({ username, text, userChoice, classes }) => (
  <div className={classNames(classes.container, 'comments')}>
    <div className={classNames(classes.commentUser, classes.left)}> {username} ({userChoice}) </div>
    <div className={classNames(classes.commentText, classes.right)}> {text} </div>
  </div>
);

const CommentList = ({ poll, comments, classes }) => (
  <div>
    {comments.map(comment =>
      poll._id === comment.pollId
        ? <Comment key={comment._id} username={comment.user.username} text={comment.text} userChoice={comment.userChoice.desc} classes={classes} />
        : '')}
  </div>
);

export default CommentList;