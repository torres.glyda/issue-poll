import React from 'react';
import classNames from 'classnames';

const Poll = ({ user, poll, selectChoice, classes }) => (
	<div className={classNames(classes.container, classes.pollContainer)}>
		<div className={classes.textHeader}>{poll.question} ({poll.totalResponses} responses)</div>
		<div>
			{poll.choices.map(choice =>
				<label key={`${choice.desc}`} className='choices'>
					<input type='radio' name='choices' onClick={selectChoice(poll, choice)} />
					<span> {choice.desc} </span>
					<span> ({choice.numberOfResponse}, {choice.responsePercentage}%) </span>
					<br />
				</label>
			)}
		</div>
	</div>
);

export default Poll;