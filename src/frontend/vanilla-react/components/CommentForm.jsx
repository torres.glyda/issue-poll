import React from 'react';
import classNames from 'classnames';

const CommentForm = ({ poll, handleCommentSubmit, classes }) => (
  <form onSubmit={handleCommentSubmit(poll)}>
    <div >
      <div className={classes.textHeader}>Comments</div>
      <textarea placeholder='Add comment...' />
    </div>
    <input type='submit' value='Comment' />
  </form>
);

export default CommentForm;