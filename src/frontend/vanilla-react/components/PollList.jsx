import React from 'react';
import { Link } from 'react-router-dom';

const PollListItem = ({ poll, i, classes }) => (
	<Link to={`/vanilla-poll-${i}`} className={classes.link}>
		<div className={classes.pollListItem}>
			<span className={classes.left}>{poll.question}</span>
			<span className={classes.right}>Total responses: {poll.totalResponses}</span>
		</div>
	</Link>
);

const PollList = ({ polls, classes }) => (
	<div>
		{polls.map((poll, i) => <PollListItem poll={poll} i={i} key={poll._id} classes={classes} />)}
	</div>
);

export default PollList;