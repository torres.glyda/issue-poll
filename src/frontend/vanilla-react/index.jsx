import React from 'react';
import ReactDOM from 'react-dom';

import { ThemeProvider } from 'react-jss';
import theme from '../styles/theme';
import injectSheet from 'react-jss';
import styles from '../styles/styles';

import client from '../client';
import AppContainer from './containers/AppContainer';

const StyledApp = injectSheet(styles)(AppContainer);

(async () => {
  await client.checkLoggedInUser();
  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <StyledApp />
    </ThemeProvider>,
    document.getElementById('vanilla-react-app')
  );
})();