import React from 'react';
import { observer, inject } from 'mobx-react';
import { HashRouter as Router, Route, Link } from 'react-router-dom';

import Poll from './Poll'
import PollList from './PollList';
import CommentSection from './CommentSection';

@inject('appStore') @observer
class App extends React.Component {
	constructor(props) {
		super(props);
		this.appStore = props.appStore;
		this.classes = props.classes;
	}

	selectChoice(poll, choice) {
		this.appStore.selectChoice(poll, choice);
	}

	handleCommentSubmit() {
		this.appStore.handleCommentSubmit();
	}

	componentWillMount() {
		this.appStore.registerListeners();
	}

	componentWillUnmount() {
		this.appStore.removeListeners();
	}

	render() {
		return <Router >
			<div className={this.classes.center}>
				{!!this.appStore.user
					? <div>
						<div className={this.classes.textHeader}>Hi, {this.appStore.user.username}!</div>
						<button onClick={this.appStore.handleLogout}>LOGOUT</button >
					</div>
					: <div>
						<div className={this.classes.textHeader}>LOGIN</div>
						<span>username:</span><input type='text' onChange={this.appStore.updateInputValue('username')} />
						<span>password:</span><input type='password' onChange={this.appStore.updateInputValue('password')} />
						<button onClick={this.appStore.handleLogin}> LOGIN</button>
					</div>
				}
				<Route path='/' render={() => <PollList appStore={this.appStore} classes={this.classes} />} />
				{this.appStore.polls.map((poll, index) =>
					<Route key={poll._id} path={`/mobx-poll-${index}`} render={() =>
						<div>
							<Poll appStore={this.appStore} poll={poll} classes={this.classes} selectChoice={(selectChoice, choice) => this.selectChoice(selectChoice, choice)} className={this.classes.center} />
							<CommentSection poll={poll} appStore={this.appStore} handleCommentSubmit={() => this.handleCommentSubmit()} classes={this.classes} />
						</div>
					} />
				)}
			</div>
		</Router>
	}
}

export default App;