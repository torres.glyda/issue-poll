import React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';

const PollListItem = observer(({ appStore, poll, i, classes }) => (
	<Link to={`/mobx-poll-${i}`} className={classes.link} onClick={appStore.computeResponsePercentage(poll)}>
		<div className={classes.pollListItem}>
			<span className={classes.left}>{poll.question}</span>
			<span className={classes.right}>Total responses: {poll.totalResponses}</span>
		</div>
	</Link>
));

const PollList = observer(({ appStore, selectPoll, classes }) => (
	<div>
		{appStore.polls.map((poll, i) => <PollListItem poll={poll} i={i} key={poll._id} appStore={appStore} selectPoll={selectPoll} classes={classes} />)}
	</div>
));

export default PollList;