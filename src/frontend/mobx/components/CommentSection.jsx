import React from 'react';
import { observer } from 'mobx-react';
import CommentForm from './CommentForm';
import CommentList from './CommentList';
import classNames from 'classnames';

const CommentSection = ({ poll, appStore, classes }) => (
  <div className={classNames(classes.container, classes.commentSectionContainer)}>
    <CommentForm poll={poll} appStore={appStore} classes={classes} />
    <CommentList poll={poll} appStore={appStore} classes={classes} />
  </div>
);

export default CommentSection;