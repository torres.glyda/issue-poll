import React from 'react';
import { observer } from 'mobx-react';
import classNames from 'classnames';

const Poll = observer(({ poll, appStore, selectChoice, classes }) => (
  <div className={classNames(classes.container, classes.pollContainer)} >
    <div className={classes.textHeader}>{poll.question} ({poll.totalResponses} responses)</div>
    <div>
      {appStore.choices.map(choice =>
        choice.pollId === poll._id
          ? <label key={`${choice.desc}`} className='choices'>
            <input type='radio' name='choices' onClick={() => selectChoice(poll, choice)} />
            <span> {choice.desc} </span>
            <span> ({choice.numberOfResponse}, {choice._responsePercentage}%) </span>
            <br />
          </label>
          : ''
      )}
    </div>
  </div>
));

export default Poll;