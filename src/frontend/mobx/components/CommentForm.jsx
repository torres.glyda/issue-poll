import React from 'react';
import classNames from 'classnames';

const CommentForm = ({ poll, appStore, onCommentSubmit, classes }) => (
  <form onSubmit={appStore.handleCommentSubmit(poll)}>
    <div >
      <div className={classes.textHeader}>Comments</div>
      <textarea placeholder='Add comment...' />
    </div>
    <input type='submit' value='Comment' />
  </form>
);

export default CommentForm;