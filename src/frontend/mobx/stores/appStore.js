import { observable, action } from 'mobx';
import client from '../../client';

class AppStore {
  @observable polls = [];
  @observable comments = [];
  @observable username = '';
  @observable password = '';
  @observable user = {};
  @observable choices = [];

  @action async retrievePolls() {
    try {
      const polls = await client.service('api/polls').find();
      this.polls = polls;
      this.polls.forEach(poll => poll.choices.map(choice => {
        choice.pollId = poll._id;
        this.choices.push(choice);
      }));
    } catch (error) {
      console.log(error);
    }
  }

  @action async retrieveComments() {
    try {
      const comments = await client.service('api/comments').find();
      this.comments = comments;
    } catch (error) {
      console.log(error);
    }
  }

  @action async setUser() {
    this.user = client.get('user');
  }

  @action.bound async handleLogin() {
    try {
      const result = await client.authenticate({
        strategy: 'local',
        username: this.username,
        password: this.password,
      });
      const payload = await client.passport.verifyJWT(result.accessToken);
      const user = await client.service('users').get(payload.userId);
      client.set('user', user);
      this.user = user;
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound async handleLogout() {
    try {
      await client.logout();
      client.set('user', undefined);
      this.user = undefined;
    } catch (err) {
      console.log('LOGOUT_FAILED');
      console.log(err);
    }
  }

  @action.bound updateInputValue(input) {
    return (evt) => {
      this[input] = evt.target.value;
    }
  }

  @action computeResponsePercentage(poll) {
    poll.computeResponsePercentage();
  }

  userHasVoted() {
    return (userId) => userId === this.user._id;
  }

  updatePollChoices(updatedChoice) {
    this.choices = this.choices.map(choice => {
      if (choice._id === updatedChoice._id) {
        Object.assign(choice, updatedChoice);
      }
      return choice;
    })
  }

  @action selectChoice(poll, selectedChoice) {
    if (!this.user) {
      alert('Login first!');
      return;
    }
    this.choices.forEach((choice) => {
      const userIdIndex = choice.userIds.findIndex(this.userHasVoted());
      if (userIdIndex !== -1) {
        choice.userIds.splice(userIdIndex, 1);
        this.updateChoice(choice._id, choice.userIds);
      } else if (choice._id === selectedChoice._id) {
        choice.userIds.push(this.user._id);
        this.updateChoice(choice._id, choice.userIds);
      }
      this.computeResponsePercentage(poll);
    })
  }

  @action async updateChoice(id, userIds) {
    try {
      await client.service('api/choices').patch(id, { userIds });
    } catch (error) {
      console.log(error)
    }
  }

  @action getUserChoice(poll) {
    const user = this.user;
    for (let i = 0; i < poll.choices.length; i++) {
      const choice = poll.choices[i];
      for (let j = 0; j < choice.userIds.length; j++) {
        const userId = choice.userIds[j];
        if (userId === user._id) {
          return choice;
          break;
        }
      }
    }
  }

  updateComments(comment) {
    this.comments.push(comment);
  }

  @action.bound handleCommentSubmit(poll) {
    return (evt) => {
      const user = this.user;
      if (!!user) {
        const text = evt.target[0].value.trim();
        const userChoice = this.getUserChoice(poll);
        if (!text || !userChoice) {
          return;
        }
        this.addComment(poll._id, user._id, userChoice._id, text);
        evt.target[0].value = '';
      } else {
        alert('Login first!');
      }
    }
  }

  async addComment(pollId, userId, userChoiceId, text) {
    try {
      await client.service('api/comments').create({ pollId, userId, userChoiceId, text });
    } catch (error) {
      console.log(error)
    }
  }

  registerListeners() {
    client.service('api/choices').on('patched', choice => {
      this.updatePollChoices(choice);
    })
    client.service('api/comments').on('created', comment => {
      this.updateComments(comment);
    })
  }

  removeListeners() {
    client.service('api/choices').off('patched');
    client.service('api/comments').off('created');
  }
}

export default new AppStore();