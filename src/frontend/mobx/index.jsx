import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { HashRouter as Router } from 'react-router-dom';

import { ThemeProvider } from 'react-jss';
import theme from '../styles/theme';
import injectSheet from 'react-jss';
import styles from '../styles/styles';

import App from './components/App';
import appStore from './stores/appStore';

import client from '../client';

const store = async () => {
  await appStore.retrievePolls();
  await appStore.retrieveComments();
}

const StyledApp = injectSheet(styles)(App);

(async () => {
  await store();
  await client.checkLoggedInUser();
  appStore.setUser();
  ReactDOM.render(
    <Provider appStore={appStore} >
      <ThemeProvider theme={theme}>
        <StyledApp />
      </ThemeProvider>
    </Provider >,
    document.getElementById('mobx-app')
  );
})();