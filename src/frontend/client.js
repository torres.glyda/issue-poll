import feathers from 'feathers/client';
import feathersSocketio from 'feathers-socketio';
import socketioClient from 'socket.io-client';
import authClient from 'feathers-authentication-client';
import hooks from 'feathers-hooks';
import clientServices from './clientServices';

const socketConnection = socketioClient('http://localhost:8000');
const client = feathers();

client
  .configure(feathersSocketio(socketConnection))
  .configure(authClient({ storage: localStorage }))
  .configure(hooks())
  .configure(clientServices());

client.checkLoggedInUser = async() => {
  try {
    const response = await client.authenticate();
    const payLoad = await client.passport.verifyJWT(response.accessToken);
    const user = await client.service('users').get(payLoad.userId)
    client.set('user', user);
  } catch (err) {
    console.log(err)
  }
}

window.app = client;

export default client;