const theme = {
	font: 'Roboto, sans-serif',
	color1: 'midnightblue',
	color2: 'orange',
	color3: 'lightskyblue',
	color4: 'ghostwhite',
	color5: 'lemonchiffon',
	color6: 'mintcream',
}

export default theme;