const styles = theme => ({
  '@global': {
    body: {
      fontFamily: theme.font,
      background: theme.color3,
      position: 'absolute',
      top: '0',
      bottom: '0',
      left: '0',
      right: '0',
    },
    textArea: {
      fontFamily: theme.font,
      width: '70%',
      height: '70px',
      resize: 'none',
      overflow: 'auto',
      margin: '0',
      padding: '10px',
    },
    'input[type=submit], button': {
      cursor: 'pointer',
      padding: '10px 20px',
      borderRadius: '5px',
      textDecoration: 'none',
      background: theme.color1,
      color: theme.color4,
      fontSize: '15px',
      outline: 'none',
      position: 'relative',
      display: 'inline-block',
      transitionDuration: '0.4s',
      '&:hover': {
        background: theme.color3,
      },
    },
    'textarea, input[type=text]': {
      outline: 'none',
      border: 'none',
      borderRadius: '2px',
      boxShadow: '0px 1px 5px rgba(0,0,0,0.3)',
      background: theme.color4,
      '&:focus': {
        background: theme.color6,
      },
    },
    '.comments': {
      marginTop: '20px',
    }
  },
  textHeader: {
    fontSize: '25px',
    margin: '10px auto 10px',
    color: theme.color1,
  },
  container: {
    display: 'inline-block',
    width: '80%',
    padding: '10px 20px 20px 20px',
    background: theme.color4,
    boxShadow: '0px 1px 5px rgba(0,0,0,0.3)',
  },
  pollContainer: {
    marginTop: '50px',
    borderRadius: '5px 5px 0 0',
    '& .choices': {
      cursor: 'pointer',
      '&:hover': {
        color: theme.color2,
      },
    },
  },
  commentSectionContainer: {
    borderRadius: '0 0 5px 5px',
    marginBottom: '15px',
  },
  commentUser: {
    width: '100px',
    marginBottom: '10px',
    fontWeight: 'bolder',
    color: theme.color1,
  },
  commentText: {
    minWidth: '100%',
    maxWidth: '500px',
  },
  pollListItem: {
    fontSize: '15px',
    height: '50px',
    color: theme.color1,
    margin: '10px 25px 10px 25px',
    padding: '25px 10px 0 10px',
    background: theme.color4,
    '&:hover': {
      color: theme.color2,
      background: theme.color5,
    },
  },
  link: {
    textDecoration: 'none',
  },
  center: {
    textAlign: 'center',
    margin: '0 auto',
  },
  left: {
    float: 'left',
  },
  right: {
    float: 'right',
  },
})

export default styles;