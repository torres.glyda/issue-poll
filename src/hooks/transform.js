const transform = (Model) => async(hook) => {
  const details = hook.type === 'after' ? 'result' : 'data';
  let hookData = hook[details];
  if (hookData instanceof Array) {
    hook[details] = hookData.map(objectData => new Model(objectData));
  } else if (hookData instanceof Object) {
    hook[details] = new Model(hookData);
  }
  return Promise.resolve(hook);
}

export default transform;