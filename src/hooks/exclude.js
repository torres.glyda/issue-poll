const exclude = (objectData) => (hook) => {
  if (hook.method === 'create' || hook.method === 'patch') {
    delete hook.result[objectData];
  } else {
    hook.result.map(object => delete object[objectData]);
  }
  return Promise.resolve(hook);
}

export default exclude;