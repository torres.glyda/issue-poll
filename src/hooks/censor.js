const getCensoredSubstring = (substring) => {
  const censorship = '*';
  let censoredSubstring = '';
  for (let x = 0; x < substring.length; x++) {
    censoredSubstring += censorship;
  }
  return censoredSubstring;
}

const censor = (mainField, parentField) => (hook) => {
  let string = '';
  if (hook.method === 'create' || hook.method === 'patch') {
    string = hook.result[parentField][mainField];
    const stringLength = string.split('').length;
    const substring = string.substring(2, stringLength - 2);
    const censoredSubstring = getCensoredSubstring(substring);
    const censoredString = string.replace(substring, censoredSubstring);
    hook.result[parentField][mainField] = censoredString;
  } else {
    hook.result.map(object => {
      string = object[parentField][mainField];
      const stringLength = string.split('').length;
      const substring = string.substring(2, stringLength - 2);
      const censoredSubstring = getCensoredSubstring(substring);
      const censoredString = string.replace(substring, censoredSubstring);
      object[parentField][mainField] = censoredString;
    })
  }
  return Promise.resolve(hook);
}

export default censor;