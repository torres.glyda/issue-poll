const populate = (service, parentField, nameAs) => async(hook) => {
  let resultData = hook.result;
  let query;
  let queryItem;
  let retrievedData;
  if (hook.method === 'create' || hook.method === 'patch') {
    queryItem = resultData[parentField];
    query = {
      _id: queryItem
    }
    retrievedData = await hook.app.service(service).find({ query });
    resultData[nameAs] = retrievedData[0];
  } else {
    for (let data of resultData) {
      if (!!data[parentField].length) {
        queryItem = data[parentField].map(parentFieldDataId => (parentFieldDataId));
        query = {
          _id: {
            $in: queryItem
          }
        }
        retrievedData = await hook.app.service(service).find({ query });
        data[nameAs] = retrievedData;
      } else {
        queryItem = data[parentField];
        query = {
          _id: queryItem
        }
        retrievedData = await hook.app.service(service).find({ query });
        data[nameAs] = retrievedData[0];
      }

    }
  }
  return Promise.resolve(hook);
}

export default populate;