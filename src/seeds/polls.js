import service from './service';

const seed = async() => {
  const pollsPath = '/api/polls';
  const pollsService = await service(pollsPath);
  const choicesService = await service('api/choices');
  const choices = await choicesService.find({})
  const choicesIds = choices.map(choice => choice._id);
  const polls = [{
      question: 'Is this a question?',
      choicesIds: choicesIds.slice(0, 4),
    },
    {
      question: 'How are you?',
      choicesIds: choicesIds.slice(4, 8),
    },
  ];

  await pollsService.remove(null);
  return pollsService.create(polls);
};

export default seed;