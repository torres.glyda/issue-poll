import service from './service';

const seed = async() => {
  const usersService = await service('users');
  const users = [{
      username: 'disbemyuname',
      password: 'disbemypword',
    },
    {
      username: 'anonymoose',
      password: 'moomoo',
    },
    {
      username: 'doog',
      password: 'catismyfriend',
    },
  ];
  await usersService.remove(null);
  return usersService.create(users);
};

export default seed;