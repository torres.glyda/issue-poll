import service from './service';

const seed = async() => {
  const choicesPath = '/api/choices';
  const choicesService = await service(choicesPath);
  const choices = [{
    desc: 'Yes',
    userIds: [],
  }, {
    desc: 'No',
    userIds: [],
  }, {
    desc: 'Maybe',
    userIds: [],
  }, {
    desc: 'idk',
    userIds: [],
  }, {
    desc: 'OK.',
    userIds: [],
  }, {
    desc: 'Okay.',
    userIds: [],
  }, {
    desc: 'Okay?',
    userIds: [],
  }, {
    desc: 'Okaaaaay.',
    userIds: [],
  }, ];

  await choicesService.remove(null);
  return choicesService.create(choices);
};

export default seed;