import setupServer from '../backend/setupServer';

const setupService = async(path) => {
  const app = await setupServer();
  const service = app.service(path);
  return service;
}

export default setupService;