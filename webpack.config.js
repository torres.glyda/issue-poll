const path = require('path');
const webpack = require('webpack');

const config = {
  cache: true,
  entry: {
    mobxapp: [
      'babel-polyfill',
      'webpack-hot-middleware/client',
      './src/frontend/mobx/index.jsx',
    ],
    vanillareactapp: [
      'babel-polyfill',
      'webpack-hot-middleware/client',
      './src/frontend/vanilla-react/index.jsx',
    ]
  },
  output: {
    path: path.join(process.cwd(), 'public/js'),
    publicPath: '/js',
    filename: '[name].js',
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      use: [{
          loader: 'react-hot-loader'
        },
        {
          loader: 'babel-loader'
        }
      ],
      exclude: /node_modules/,
    }, ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ]
};

module.exports = config;