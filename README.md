# **React State Management vs. Mobx**

----------
# Performance

In React's state management, states are changed or added trough *setState*. Although, calling *setState* does not instantly change the *this.state*, and when trying to access it after, it returns the value it currently has. If there's a need for a state change, oftentimes the state is slow and not appearing right away in local components when rendering. There could also be a possibility that a change of state can affect irrelevant states when re-rendering.

Components are usually divide into two, *container* and *presentational*. This helps in maintaining and reusing components easier. States are also easier to handle.

Mobx's state management is simpler compared to React's. In Mobx, *observables*, *observers*, etc., are used to manage states through stores. Whenever there are changes made, it is immediately shown in local components. It also manages states that are only needed when rendering and won't affect other states. 

Using Mobx, components does not necessarily need to have presentational components, as states are managed through stores.

----------
# Testability

Testing is simpler in Mobx. Mobx classes and stores acts like objects, which are simpler to handle. When actions are called, observers immediately receives any changes that were done in the store. React components that are observed can be easily tested, because once an observable within a store has changed, the components will have changed as well right away. Testing with React, with its states alone, once a state is called to be change, it does not immediately reflect on the components where the state is supposed be.

----------
# Modifiability

React *states* are modified by using *setState* and is called by *this.state*. Everytime a state needs to be changed, *setState* is always called to change the current value of a state.

Mobx has *stores* and has its states act as *observables*. If there's a need for *observables* to be modified, the new value will just have to be equated to it, the same with setting new values to variables.
